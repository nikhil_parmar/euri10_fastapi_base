#!/usr/bin/env bash

set -euxo pipefail

sh scripts/lintcheck.sh

docker-compose -f docker-compose-build.yml build
docker-compose -f docker-compose-build.yml down -v --remove-orphans
docker-compose -f docker-compose-build.yml up -d
sleep 20
docker-compose -f docker-compose-build.yml exec -T backend-tests pytest tests -v

