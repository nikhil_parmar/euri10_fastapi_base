#!/usr/bin/env bash

set -euxo pipefail

TAG=${TAG} docker-compose -f docker-compose-build.yml build

TAG=${TAG} docker-compose -f docker-compose-build.yml push