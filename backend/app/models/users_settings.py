from enum import Enum

import sqlalchemy
from pydantic import BaseModel, UrlStr
from sqlalchemy.dialects.postgresql import JSONB, UUID

from models import metadata

users_settings = sqlalchemy.Table(
    "users_settings",
    metadata,
    sqlalchemy.Column("id", sqlalchemy.Integer, primary_key=True),
    sqlalchemy.Column(
        "users_id", UUID(), sqlalchemy.ForeignKey("users.id"), nullable=False
    ),
    sqlalchemy.Column("settings", JSONB(), nullable=False),
    sqlalchemy.CheckConstraint(
        "check_all_json_key(settings)", name="check_all_json_key"
    ),
)


class AvailableLang(str, Enum):
    en = "en"
    fr = "fr"
    de = "de"


class AvailableTheme(str, Enum):
    dark = "dark"
    light = "light"


class UserSettingsIn(BaseModel):
    lang: AvailableLang
    theme: AvailableTheme
    avatar: UrlStr

    class Config:
        use_enum_values = True
        extra = "forbid"
